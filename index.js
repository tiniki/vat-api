const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const fs = require('fs')
const path = require('path')
const stringify = require('./stringify')

const PORT = process.env.NODE_PORT || 3000
const DB = process.env.DB_PATH || 'mongodb://localhost/vat'

mongoose.connect(
	DB,
	{ useNewUrlParser: true },
	error => (error ? console.error(`Connect db failed: ${error}`) : console.log('Connect db successfully')),
)

const app = express()
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(morgan('combined', { stream: accessLogStream }))

app.get('/', (req, res) => {
	res.setHeader('Content-Type', 'application/json')
	res.end(stringify(mongoose.connection))
})

app.listen(PORT, () => console.log(`Api listening on port ${PORT}!`))
