module.exports = {
	"extends": [
		"airbnb-base"
	],
	"rules": {
		"no-unused-vars": "warn",
		"arrow-parens": ["warn", "as-needed"],
		"indent": [ "warn", "tab", { "SwitchCase": 1 } ],
		"no-tabs": [ "warn", { "allowIndentationTabs": true } ],
		"semi": [ "warn", "never" ],
		"comma-dangle": [
			"warn",
			{
				"arrays": "always-multiline",
				"objects": "always-multiline",
				"imports": "always-multiline",
				"exports": "always-multiline",
				"functions": "ignore"
			}
		],
		"no-unused-expressions": [ "warn", { "allowShortCircuit": true, "allowTernary": true }],
		"max-len": [ "warn", 120 ],
		"dot-notation": "warn",
		"spaced-comment": "warn",
	}
}
